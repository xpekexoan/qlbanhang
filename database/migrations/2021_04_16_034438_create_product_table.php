<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->decimal('price', 10, 0);
            $table->integer('qty')->default(0);
            $table->string('images');
            $table->double('discount', 4, 2);
            $table->integer('status')->default(1)->comment('0.Hide | 2.Show');
            $table->unsignedInteger('id_category_sub');
            $table->timestamps();
            $table->foreign('id_category_sub')->references('id')->on('category_sub');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
