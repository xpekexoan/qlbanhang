<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_info', function (Blueprint $table) {
            $table->unsignedBigInteger('id_product');
            $table->string('screen')->nullable();
            $table->string('ram')->nullable();
            $table->string('pin')->nullable();
            $table->string('os')->nullable();
            $table->string('weight')->nullable();
            $table->text('description')->nullable();
            $table->text('other_info')->nullable();
            $table->primary('id_product');
            $table->foreign('id_product')->references('id')->on('product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_info');
    }
}
