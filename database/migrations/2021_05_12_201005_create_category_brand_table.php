<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryBrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_brand', function (Blueprint $table) {
            $table->unsignedSmallInteger('id_category');
            $table->unsignedInteger('id_brand');
            $table->primary(['id_category', 'id_brand']);
            $table->foreign('id_category')->references('id')->on('category');
            $table->foreign('id_brand')
                    ->references('id')->on('brand')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_brand');
        // Schema::disableForeignKeyConstraints();
        // Schema::enableForeignKeyConstraints();
    }
}
