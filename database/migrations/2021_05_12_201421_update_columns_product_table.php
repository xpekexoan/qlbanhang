<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            // Brand
            $table->unsignedInteger('id_brand')->nullable();
            $table->foreign('id_brand')
                    ->references('id')->on('brand')
                    ->onDelete('set null');
                    
            // CategorySub
            if (Schema::hasColumn('product', 'id_category_sub')) {
                $table->dropForeign('product_id_category_sub_foreign');
                $table->dropColumn('id_category_sub');
            }
            $table->unsignedInteger('id_cat_sub')->nullable();
            $table->foreign('id_cat_sub')
                    ->references('id')->on('category_sub')
                    ->onDelete('set null');
                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->dropForeign('product_id_brand_foreign');
            $table->dropColumn('id_brand');
            $table->dropForeign('product_id_cat_sub_foreign');
            $table->dropColumn('id_cat_sub');
        });
    }
}
