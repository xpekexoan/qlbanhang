<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_cus');
            $table->unsignedBigInteger('id_shipper');
            $table->string('tel', 10);
            $table->string('address');
            $table->decimal('total_money', 10, 0);
            $table->dateTime('order_date');
            $table->dateTime('recv_date');
            $table->integer('status')->default(0)->comment('0. Chờ duyệt | 1. Đã duyệt & Chờ giao | 2. Đang giao hàng | 4. Đã nhận hàng');

            $table->foreign('id_cus')->references('id')->on('users');
            $table->foreign('id_shipper')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
