<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropIdSuppProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->dropForeign('product_id_supp_foreign');
            $table->dropColumn('id_supp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->unsignedMediumInteger('id_supp')->nullable();
            $table->foreign('id_supp')
                    ->references('id')->on('supplier')
                    ->onDelete('set null');
        });
    }
}
