<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            'name' => 'xpekexoan',
            'email' => 'xpekexoan@gmail.com',
            'password' => bcrypt('123123'),
            'id_role' => '1'
        ];
        DB::table('users')->insert($users);
    }
}
